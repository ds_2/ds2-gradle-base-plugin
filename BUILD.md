# DS/2 Base Plugin

## How to build and test

This project is a composite build project. In the directory base-plugin you will find the plugin itself. In the base-plugin-test directory you will find a test project to allow testing the plugin directly.

In the test module you will find an additional README file. Please read it to get familiar with testing.

## Release plugin

Merge everything you want to release to the branch
release/0.1. Change to this branch and run:

    cd base-plugin
    javac -version # please check java version!! It should be Java 8!!
    gradle :clean :release -Prelease.useAutomaticVersion=true -Prelease.newVersion=1.1.0-SNAPSHOT -Prelease.releaseVersion=1.0.0

Please change the release version appropriately.
