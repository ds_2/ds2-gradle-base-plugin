pluginManagement {
    repositories {
        mavenLocal()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "DS2 Base Plugin Composite"

includeBuild("base-plugin")
includeBuild("base-plugin-test")
