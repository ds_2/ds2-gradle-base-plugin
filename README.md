# DS/2 Gradle Base Plugin

The base plugin for all DS2 projects. Comes with a bunch of preinstalled
configurations and tasks.

https://plugins.gradle.org/plugin/ds2.base


# Docs

* https://guides.gradle.org/implementing-gradle-plugins/

## Usage

Please see [here](USAGE.md).

## How to build the plugin.

Please see [here](BUILD.md).
