package ds2.gradle.plugins.base

import org.gradle.api.JavaVersion

open class Ds2BasePluginExtension(
    /**
     * The java version to use.
     */
    var javaVersion: JavaVersion = JavaVersion.VERSION_1_8,
    /**
     * Crash if the java version in the runtime is not the one you configured.
     */
    var crashIfUnconfigured: Boolean = true,
    /**
     * Use Gradle 6.7 toolchain to set version instead of target/source.
     */
    var useToolchain: Boolean = true,
    var jvmVendor: String = ""
) : java.io.Serializable {
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("(")
        sb.append("javaVersion=").append(javaVersion)
        sb.append(", crashIfUnconfigured=").append(crashIfUnconfigured)
        sb.append(", useToolchain=").append(useToolchain)
        sb.append(", jvmVendor=").append(jvmVendor)
        sb.append(")")
        return sb.toString()
    }

    companion object {
        internal const val serialVersionUID = 727566175075960653L
    }
}
