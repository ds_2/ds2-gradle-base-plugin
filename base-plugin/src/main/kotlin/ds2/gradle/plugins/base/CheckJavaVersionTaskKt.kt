package ds2.gradle.plugins.base

import org.gradle.api.DefaultTask
import org.gradle.api.JavaVersion
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

open class CheckJavaVersionTaskKt : DefaultTask() {
    @Input
    var requiredProjectVersion: JavaVersion = JavaVersion.VERSION_1_8
    init {
        description = "Checks the project java version against the required (configured) java version"
        group = "Build"
    }

    @TaskAction
    fun checkProjectJavaVersion() {
        logger.info("Running task now..")
        val thisJavaVersion = JavaVersion.current()
        logger.debug("User runs with java version {}", thisJavaVersion)
        logger.debug("The configuration of the project says we should target java {}", requiredProjectVersion)
        if (thisJavaVersion > requiredProjectVersion) {
            logger.warn("The installed java version is newer than the required version. There MAY be issues as the installed java now has to run in compatibility mode!")
        } else if (thisJavaVersion != requiredProjectVersion) {
            throw IllegalStateException("ERROR: Java $requiredProjectVersion required but $thisJavaVersion found. Please check your JAVA_HOME environment, or PATH as well.")
        }
        logger.debug("Done with task.")
    }
}
