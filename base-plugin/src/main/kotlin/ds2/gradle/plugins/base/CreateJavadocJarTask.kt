package ds2.gradle.plugins.base

import org.gradle.api.tasks.bundling.Jar

open class CreateJavadocJarTask: Jar() {
    init {
        dependsOn("javadoc")
        archiveClassifier.set("javadoc")
        description = "puts all the javadoc files in an archive"
        from("build/docs/javadoc")
    }
}
