package ds2.gradle.plugins.base

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.jvm.toolchain.JvmVendorSpec
import org.gradle.util.GradleVersion

open class BasePluginKt : Plugin<Project> {

    companion object {
        const val TASK_NAME_CHECK_JAVA_VERSION = "checkJavaVersion"
        const val TASK_NAME_CREATE_SRC_JAR = "srcJar"
        const val TASK_NAME_CREATE_JAVADOC_JAR = "docJar"
        const val TASK_EXTENSION_NAME = "baseConfig"
    }

    override fun apply(target: Project) {
        if (GradleVersion.current() < GradleVersion.version("7.0.0")) {
            target.logger.warn("Your Gradle version is out-of-date. Please consider to upgrade!!")
        }
        with(target) {
            logger.info("Hi, I will now prepare this project ${this.name} for usage with me..")
            plugins.apply("base")
            setupCdiConfiguration(this)
            val config = extensions.create(TASK_EXTENSION_NAME, Ds2BasePluginExtension::class.java)
            logger.info("Types: ${this.plugins}")
            plugins.withType(JavaPlugin::class.java).whenPluginAdded {
                logger.info("Applying tasks for java project..")
//                tasks.register("checkJavaVersion", CheckJavaVersionTaskKt::class.java) {
//                    it.requiredProjectVersion = config.javaVersion
//                }
//                tasks.register("createSourceJar", CreateSourceJarTask::class.java)
//                tasks.register("createJavadocJar", CreateJavadocJarTask::class.java)
            }
            plugins.withId("java") {
                logger.info("Seems to be a java project?? Add (again) my tasks..")
                tasks.register(TASK_NAME_CHECK_JAVA_VERSION, CheckJavaVersionTaskKt::class.java) {
                    it.requiredProjectVersion = config.javaVersion
                }
                tasks.register(TASK_NAME_CREATE_SRC_JAR, CreateSourceJarTask::class.java)
                tasks.register(TASK_NAME_CREATE_JAVADOC_JAR, CreateJavadocJarTask::class.java)
                tasks.getByName(JavaPlugin.COMPILE_JAVA_TASK_NAME) {
                    it.dependsOn(TASK_NAME_CHECK_JAVA_VERSION)
                }
                this.afterEvaluate {
                    val javaExtension = this.extensions.getByType(JavaPluginExtension::class.java)
                    logger.info("JavaSrcCompat is set to ${javaExtension.sourceCompatibility}")
                    logger.info("Setting JavaSrcCompat to ${config.javaVersion}..")
                    if (config.useToolchain) {
                        val thisToolChain = javaExtension.toolchain
                        thisToolChain.languageVersion.set(JavaLanguageVersion.of(config.javaVersion.majorVersion))
                        if (config.jvmVendor.isNotEmpty()) {
                            val jvmVendor = JvmVendorSpec.matching(config.jvmVendor)
                            logger.info("Using vendor ${jvmVendor}..")
                            thisToolChain.vendor.set(jvmVendor)
                        }
                    } else {
                        javaExtension.sourceCompatibility = config.javaVersion
                        javaExtension.targetCompatibility = config.javaVersion
                    }
                }
            }
            logger.info("Done with target :)")
        }
    }

    private fun setupCdiConfiguration(prj: Project) {
        prj.plugins.withType(JavaPlugin::class.java).whenPluginAdded {
            val cdiTestConfig = prj.configurations.create("cdiTest")
            //cdiTestConfig.setExtendsFrom(prj.configurations.getByName("testCompile"))
            cdiTestConfig.description = "A configuration for test cases using CDI"
        }
    }
}
