package ds2.gradle.plugins.base

import org.gradle.api.tasks.bundling.Jar

open class CreateSourceJarTask : Jar() {
    init {
        dependsOn("classes")
        archiveClassifier.set("sources")
        from("sourceSets.main.allJava")
        description = "bundles all the java sources into a jar file"
    }
}
