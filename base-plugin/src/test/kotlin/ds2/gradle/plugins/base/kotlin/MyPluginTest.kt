package ds2.gradle.plugins.base.kotlin

import ds2.gradle.plugins.base.BasePluginKt
import ds2.gradle.plugins.base.Ds2BasePluginExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.JavaPlugin
import org.gradle.testfixtures.ProjectBuilder
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.fail

class MyPluginTest {
    @Test
    fun pluginHasTask() {
        val prj = ProjectBuilder.builder().build()
        prj.pluginManager.apply(BasePluginKt::class.java)
        assertNotNull(prj.extensions.getByName("base"), "Project has not the base extension!!")
    }

    @Test
    fun checkJavaVersionTask() {
        val prj = ProjectBuilder.builder().build()
        prj.pluginManager.apply(BasePluginKt::class.java)
        prj.pluginManager.apply(JavaPlugin::class.java)
        val baseConfig = prj.extensions.getByType(Ds2BasePluginExtension::class.java)
        assertNotNull(baseConfig, "No base config found!")
        val checkTask = prj.tasks.getByPath(":checkJavaVersion")
        checkTask.actions.get(0).execute(checkTask)
        assertEquals(baseConfig.javaVersion, JavaVersion.VERSION_1_8, "Expected Java 1.8 somehow..")
    }

    @Test(expectedExceptions = [IllegalStateException::class])
    fun checkDifferentJavaVersionTask() {
        val prj = ProjectBuilder.builder().build()
        prj.pluginManager.apply(BasePluginKt::class.java)
        prj.pluginManager.apply(JavaPlugin::class.java)
        val baseConfig = prj.extensions.getByType(Ds2BasePluginExtension::class.java)
        assertNotNull(baseConfig, "No base config found!")
        baseConfig.javaVersion = JavaVersion.VERSION_12
        baseConfig.crashIfUnconfigured = true
        val checkTask = prj.tasks.getByPath(":checkJavaVersion")
        assertNotNull(checkTask, "Task was not found??")
        checkTask.actions[0].execute(checkTask)
        assertEquals(baseConfig.javaVersion, JavaVersion.VERSION_12, "Expected Java 11 somehow..")
    }

    @Test(enabled = false)
    fun testJavaSourcesJarTask() {
        val prj = ProjectBuilder.builder().build()
        prj.pluginManager.apply(BasePluginKt::class.java)
        prj.pluginManager.apply(JavaPlugin::class.java)
        val baseConfig = prj.extensions.getByType(Ds2BasePluginExtension::class.java)
        assertNotNull(baseConfig, "No base config found!")
        val buildPath = prj.projectDir.toPath()
        val packageDir = buildPath.resolve("src").resolve("main").resolve("java").resolve("mypkg")
        Files.createDirectories(packageDir)
        val myJavaFile = packageDir.resolve("TestMe.java")
        val resourceToLoad = "test1.properties"
        var content = MyPluginTest::class.java.getResource(resourceToLoad)
        if (content == null) {
            content = this::class.java.getResource(resourceToLoad)
        }
        if (content == null) {
            content = this::class.java.classLoader.getResource(resourceToLoad)
        }
        if (content == null) {
            content = javaClass.getResource(resourceToLoad)
        }
        if (content == null) {
            content = object {}.javaClass.getResource(resourceToLoad)
        }
        assertNotNull(content, "Could not read the test file..")
        val contentStr = content.readText(StandardCharsets.UTF_8)
        val bw = Files.newBufferedWriter(myJavaFile, StandardCharsets.UTF_8)
        bw.write(contentStr)
        bw.close()
        baseConfig.javaVersion = JavaVersion.VERSION_11
        baseConfig.crashIfUnconfigured = false
        executeSingleTask(prj, ":build")
        executeSingleTask(prj, ":createSourceJar")
    }

    fun executeSingleTask(prj: Project, path: String): Task {
        val tsk = prj.tasks.getByPath(path)
        if (tsk.actions.size > 0) {
            tsk.actions.get(0).execute(tsk)
        } else {
            fail("Could not find a task with path $path")
        }
        return tsk
    }
}
