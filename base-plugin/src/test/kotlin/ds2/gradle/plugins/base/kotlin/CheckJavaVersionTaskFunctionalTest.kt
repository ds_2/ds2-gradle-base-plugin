package ds2.gradle.plugins.base.kotlin

import ds2.gradle.plugins.base.BasePluginKt.Companion.TASK_EXTENSION_NAME
import ds2.gradle.plugins.base.BasePluginKt.Companion.TASK_NAME_CHECK_JAVA_VERSION
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.testng.annotations.Test
import java.nio.file.Files
import kotlin.test.assertEquals

class CheckJavaVersionTaskFunctionalTest {
    @Test
    fun testTask() {
        val tmpDir = Files.createTempDirectory("ds2baseplugintest")
        val buildGradleFile = tmpDir.resolve("build.gradle")
        buildGradleFile.toFile().writeText(
            "plugins {\n" +
                    "id(\"ds2.base\")\n" +
                    "id(\"java\")\n" +
                    "}\n"
        )
        val result = GradleRunner.create()
            .withProjectDir(tmpDir.toFile())
            .withArguments(TASK_NAME_CHECK_JAVA_VERSION)
            .withPluginClasspath()
            .build()
        assertEquals(TaskOutcome.SUCCESS, result.task(":$TASK_NAME_CHECK_JAVA_VERSION")!!.outcome, "Almost..")
    }

    @Test
    fun testTaskWithBadVersion() {
        val tmpDir = Files.createTempDirectory("ds2baseplugintest")
        val buildGradleFile = tmpDir.resolve("build.gradle")
        buildGradleFile.toFile().writeText(
            "plugins {\n" +
                    "id(\"ds2.base\")\n" +
                    "id(\"java\")\n" +
                    "}\n" +
                    TASK_EXTENSION_NAME + " {\n" +
                    "javaVersion = JavaVersion.VERSION_12\n" +
                    "}\n"
        )
        val result = GradleRunner.create()
            .withProjectDir(tmpDir.toFile())
            .withArguments(TASK_NAME_CHECK_JAVA_VERSION)
            .withPluginClasspath()
            .buildAndFail()
        assertEquals(TaskOutcome.FAILED, result.task(":$TASK_NAME_CHECK_JAVA_VERSION")!!.outcome, "Almost..")
    }
}
