plugins {
    kotlin("jvm") version "1.6.20-M1"
    id("java-gradle-plugin")
    id("net.researchgate.release") version "2.8.1"
    id("com.gradle.plugin-publish") version "0.12.0"
    id("com.github.spotbugs") version "4.5.0"
    id("checkstyle")
    //id("org.ysb33r.gradletest") version "2.0-beta.3"
}

description = "Dummy plugin for any DS/2 projects"

configurations {
    implementation {
        resolutionStrategy.failOnVersionConflict()
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}


repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    api(gradleApi())
//    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.20-M1")
    testImplementation("org.testng:testng:6.14.3")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-testng")

}

tasks.register("sourceJar", Jar::class.java) {
    doLast {
        //from sourceSets . main . allJava
    }
}

tasks.test {
    useTestNG()
    testLogging.showExceptions = true
}

tasks.afterReleaseBuild {
    dependsOn("publishPlugins")
}

pluginBundle {
    website = "https://gitlab.com/ds_2/ds2-gradle-base-plugin"
    vcsUrl = "https://gitlab.com/ds_2/ds2-gradle-base-plugin.git"
    description = "A base plugin to add additional checks/tasks to work with java projects/modules in Gradle"
    tags = listOf("ds2", "base", "java", "version")

}

gradlePlugin {
    plugins {
        create("basePlugin") {
            id = "ds2.base"
            implementationClass = "ds2.gradle.plugins.base.BasePluginKt"
            displayName = "DS/2 Basic Functionality Plugin"
            description = "A base plugin to add additional checks/tasks to work with java projects/modules in Gradle"
        }
    }
}

release {
    tagTemplate = "v\${version}"
    with(propertyMissing("git") as net.researchgate.release.GitAdapter.GitConfig) {
        requireBranch = "release/0.1"
    }
}

spotbugs {
    ignoreFailures.set(true)
    val f: File = project.buildDir.toPath().resolve("reports").toFile()
    reportsDir.set(f)
    effort.set(com.github.spotbugs.snom.Effort.MIN_VALUE)
    showProgress.set(true)
    reportLevel.set(com.github.spotbugs.snom.Confidence.MAX_VALUE)

    tasks.spotbugsMain {
        reports.create("html") {
            isEnabled = true
        }
        reports.create("xml") {
            isEnabled = true
        }
    }
}
