plugins {
    id("ds2.base") version "0.0.17-SNAPSHOT"
    java
}

baseConfig {
    javaVersion = JavaVersion.VERSION_11
}
