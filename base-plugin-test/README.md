# DS/2 Base Plugin Test

This project contains the SNAPSHOT version of the project so you can test your plugin BEFORE you may want to release it.

## Testing checkJavaVersion Task

Run from this directory:

    gradle clean build

This may trigger a build of the plugin itself, and then it will run these tasks. Depending on your environment, this will be successful, or may fail. This is intended.

## Testing the jar tasks

Ultimately, you can test the jar tasks as well:

    gradle srcJar docJar
