# DS/2 Base Plugin

## How to use it

Simply add this to your root build file:

    plugins {
        id 'ds2.base' version '0.0.16' //please check the version you want to use
        //or Kotlin
        id("ds2.base") version "0.0.16"
    }
    
## Configuration

There is an extension for this:

    baseConfig {
        # to set the required java version
        javaVersion = JavaVersion.VERSION_11
    }

This will configure the checkJavaVersion task which runs before any compilation. It will fail if your current JDK does not meet the requirements (say, your local jdk is Java8, and you require Java11). It will print out a warning in case your current jdk is newer than the required java version. 
